$( document ).ready(function() {
    $('#menu-toggle').click(function () {
        if($('.main-navigation').hasClass('opened')){
            $('.main-navigation').removeClass('opened');
        }else{
            $('.main-navigation').addClass('opened');
        }
    });

    $('#owl-carousel2').owlCarousel({
        "items": 8,
        "loop": true,
        "responsive": {
            0:{
                "items": 1
            },
            760:{
                "items": 4
            },
            1100:{
                "items": 8
            }
        }
    });

    $('#owl-carousel').owlCarousel({
        "items": 2,
        "loop": true,
        "responsive": {
            0:{
                "items": 1
            },
            760:{
                "items": 2
            }
        }
    });

    $("a[href='#top']").click(function() {
        $("html, body").animate({ scrollTop: 0 }, "slow");
        return false;
    });

});